# Tabla de contenido

* [Todo Baofeng DM-X y 1702](#todo-baofeng-dm-x-y-1702)
* [Visión general](#visión-general)
* [Especificaciones](#especificaciones)
* [CPS en Español](#cps-en-español)
* [Firmware](#firmware)
* [Codeplug](#codeplug)
* [Descargas](#descargas)
* [FAQ](#faq)


[Subir](#top)

# Todo Baofeng DM-X y 1702

¡Bienvenidos!

En esta web se almacena todo tipo de información sobre el Baofeng DM-X y 1702. 
Web no oficial dedicada a recopilar información sobre este walkie talkie. La información aquí recogida se aplica a los siguientes modelos: DM-X, DM-1702, DM-1702A, DM-1702B

~~La mayor parte de la información, archivos, cps, codeplug, etc. proviene del grupo de Telegram _Grupo Baofeng DM-X DM 1701/1702_. **Mis agradecimientos a los integrantes de ese grupo**.~~  **(El grupo de telegram ya no está disponible)**

[Subir](#top)

# Visión general

Baofeng DM-X es un walkie talkie digital llegado en 2019. Es banda dual, DMO, Dual time Slot REAL, Tier I & Tier II, alta calidad de audio con tecnología DMR.

Características:

1. GPS: admite posición de longitud y latitud
2. Función de grabación de voz: memoria de 1Gb incorporada para grabar voz TX y RX continuamente durante 9 horas
3. Botón de teclado más grande que dm-860 dm-5r dm-1701 dm-1702, para que pueda manejarlo fácilmente con el dedo
4. Pantalla LED a todo color más grande
5. Doble banda VHF 137-174MHz y UHF 400-480MHz
6. Función de SMS: admite SMS, edición de texto y borrador en 64 caracteres en inglés, para que se puedan enviar mensajes de texto cuando la llamada de voz no sea posible
7. Alta calidad de sonido
8. Escritura manual de frecuencia
9. Modo de alta / baja potencia
10. Batería de iones de litio de mayor capacidad: Equipada con una batería de iones de litio de 2200 mAh. Hasta 18 horas de tiempo de trabajo y 48 horas de tiempo de espera. Dura 30% más que otros walkies DMR. DM-X es ligero y práctico, perfecto para QSO largos.
11. Se pueden programar más operaciones de teclado con los nombres de canales, frecuencias TX y RX, número de canal, tonos, etc. a través del teclado. Los usuarios también pueden personalizar los botones superior y lateral para sus funciones favoritas.
12. Distancia de conversación: la distancia de conversación del Baofeng DM-X puede ser de hasta 5 km (3 millas) por debajo del rango visible. Hay otros factores que afectan el rango de conversación de una radio bidireccional, como el clima, la frecuencia exacta utilizada y las obstrucciones, etc.

[Subir](#top)

# Especificaciones

## General

| **GENERAL**                       |                                                 |
| --------------------------------- | ----------------------------------------------- |
| Rango de frecuencias (Banda dual) | <p>VHF: 136-174 MHz </p><p>UHF: 400-470 MHz</p> |
| Canales de memoria                | 1024                                            |
| Tensión de funcionamiento         | DC 7.4 V                                        |
| Estabilidad de frecuencia         | ±1.0ppm                                         |
| Medidas                           | 136\*62\*36mm (sin incluir la antena)           |
| Temperatura de funcionamiento     | -30℃-+ 60℃                                      |
| Peso                              | Aprox. 274g (incluyendo la batería y la antena) |
| Compatible con                    | Motorola Hytera DMR Radio                       |
| Batería                           | 7.4V 2200mAh/16.28Whs                           |
| Resistencia al agua               | IP45                                            |

## Transmisión

| **TRANSMISIÓN**              |                                                                               |
| ---------------------------- | ----------------------------------------------------------------------------- |
| Potencia de salida           | <p>Alto: 5 W </p><p>Bajo: 1 W</p>                                             |
| Potencia de canal adyacente  | ≤65dB ≤60dB                                                                   |
| Consumo de salida            | <p>Analógico ≤ 1.6A </p><p>Digital ≤ 0.9A</p>                                 |
| Respuesta de Audio           | + 1 \~ 3dB                                                                    |
| Consumo en Standby           | ≤0.18A                                                                        |
| Protocolo Digital            | ETSI-TS102 361-1-2-3                                                          |
| Distorsión de modulación     | ≤5%                                                                           |
| Vocoder tipo                 | AMBE + 2 TM                                                                   |
| Modulación FM/FM             | <p>16K￠F3E, 25KHz </p><p>141K￠F3E, 20KHz </p><p>11K￠F3E,12.5KHz</p>           |
| Modulación digital 4FSK/4FSK | <p>12.5 KHz para datos: 7K60FXD </p><p>12.5 KHz para datos y voz: 7K60FXE</p> |
| Señal a ruido (wide/narrow)  | <p>≥45dB 25 KHz </p><p>≥40dB 12.5 KHz</p>                                     |
| Espurias puerto de antena    | <p>9 KHz-1 GHz ≤-36dBm </p><p>1 GHz-12.75 GHz: ≤-30dBm</p>                    |

## Recepción

| RECEPCIÓN                              |                             |
| -------------------------------------- | --------------------------- |
| Sensibilidad de recepción en analógico | -122dBm (12dB SINAD)        |
| Sensibilidad de recepción en digital   | -122dBm (BER≤5 %)           |
| Potencia de audio                      | 1 W                         |
| Distorsión de audio                    | <10%                        |
| Imitación de señal                     | ≥70dB                       |
| Respuesta de audio                     | + 1 \~ 3dB                  |
| Consumo en recepción                   | ≤380mA                      |
| Intermediación (wide/narrow)           | ≥62dB/≥58dB                 |
| Selectividad de canal adyacente        | ≥65dB/≥60dB                 |
| Ruido PM                               | ≥45dB 25 KHz/≥40dB 12.5 KHz |


[Subir](#top)

# CPS en Español

## Como poner el CPS en Español

Gracias al compañero José Antonio (EA7KZ), os dejo unos vídeos con instrucciones y el archivo necesario para poder poner el CPS en Español.

### Archivos

[Pulsa aquí para descargar el archivo para poner el CPS en Español](https://gitlab.com/javifn/baofengdmx/-/raw/main/files/cps_spanish/lang_es.lng?inline=false)

### Vídeos

[Pulsa aquí para descargar el vídeo de como AÑADIR IDIOMA](https://gitlab.com/javifn/baofengdmx/-/raw/main/files/cps_spanish/A%C3%91ADIR%20IDIOMA.wmv?inline=false)

[Pulsa aquí para descargar el vídeo de como CAMBIAR IDIOMA](https://gitlab.com/javifn/baofengdmx/-/raw/main/files/cps_spanish/CAMBIAR%20IDIOMA.wmv?inline=false)

Aquí debajo la ruta donde colocar el archivo:

<img src="images/imgruta.jpeg" alt="" data-size="original">


[Subir](#top)

# Firmware

Tutorial para actualizar el firmware de tu Baofeng DM-X o 1702. Enlace a los archivos de los firmware más recientes.

## Antes de comenzar

<img src="images/warning.png" alt="" data-size="original">

ATENCIÓN: El autor de esta web no se hace responsable por cualquier daño en tu Baofeng DM-X y 1702 debido a un uso incorrecto de este tutorial.

<img src="images/warning.png" alt="" data-size="original">

PELIGRO: actualizar el firmware incorrectamente puede inutilizar por completo tu Baofeng DM-X y 1702.

## Descargas necesarias

Para instalar el firmware en el walkie necesitaras instalar el programa CPS en un PC con Windows.

También necesitarás descargar los archivos que contienen el firmware. Son 2 archivos:

* Un archivo con extensión .bin (ejemplo: 1702\_02\_v22.bin)
* Un archivo con extensión .binini (ejemplo: 1702\_02\_v22.binini)

Puedes descargar todos los programas y archivos necesarios en la sección de [DESCARGAS](#descargas).

## Pasos para actualizar el firmware de tu Baofeng DM-X y 1702

1. Apagar el walkie
2. Iniciar el programa CPS: `MD(External) Radio Program Software`
3. Conectar el cable del PC al walkie. Sirven los 2 tipos de cable: el micro-USB y el del conector doble jack para el micrófono
4.  Pulsamos, **CON EL WALKIE APAGADO**, las teclas PTT+MENÚ+# y encendemos el walkie

    <img src="images/Baofeng-DMX.png" alt="" data-size="original">
5. En el programa CPS, vamos al menú `Program/Tool`
6. Pulsar botón Load para seleccionar el archivo .bin de firmware a instalar (p.e. `1702_02_v22.bin`)  \
   <img src="images/imagen.png" alt="" data-size="original">
7. Pulsar botón `Upgrade`, esperamos a que termine y ya está actualizado el firmware\
   <img src="images/imagen (1).png" alt="" data-size="original">


[Subir](#top)

# Codeplug

Como crear un codeplug para tu Baofeng DM-X y 1702. Enlace a los codeplug cedidos por usuarios del Grupo de Telegram _Grupo Baofeng DM-X DM 1701/1702_

## Crear un codeplug

Para crear un codeplug para el walkie, adjunto aquí debajo el manual en inglés ofrecido por el fabricante:

[Programación de Codeplug (en inglés)](https://gitlab.com/javifn/baofengdmx/-/raw/main/files/DM-X%20Programming%20Guide.pdf?inline=false)

## Codeplug de ejemplo

En la sección [DESCARGAS](#descargas) tienes varios ejemplos de codeplug que podrás instalar en tu Baofeng DM-X y 1702.

[Subir](#top)

# Descargas

## CPS

### 1.0.78

<img src="images/warning.png" alt="" data-size="original">

**¡ATENCIÓN!** Algunos antivirus han detectado **esta versión 1.0.78** del programa CPS como un posible virus. Yo lo he instalado en mi PC y mi antivirus (Windows Defender) no ha encontrado ninguna amenaza.

[Pulsa aquí para descargar el CPS de esta versión](https://gitlab.com/javifn/baofengdmx/-/raw/main/files/CPS%20MD%20v1.00.78%20en.zip?inline=false)

### 1.0.66

[Pulsa aquí para descargar el CPS de esta versión](https://gitlab.com/javifn/baofengdmx/-/raw/main/files/CPS%20MD%20v1.00.66%20en.zip?inline=false)

## Firmware

### 2.2.23

Parece que ya empiezan a llegar walkies con este firmware. Un compañero del grupo anuncia que lo ha recibido en un 1702. Se supone que próximamente estará disponible para el resto de modelos.

### ​2.2.22

[Firmware 2.2.22](https://gitlab.com/javifn/baofengdmx/-/raw/main/files/1702_02_v022.zip?inline=false)

### ​2.2.19

[Firmware 2.2.19](https://gitlab.com/javifn/baofengdmx/-/raw/main/files/1702_02_v019.zip?inline=false)

## Codeplug

Estos codeplug han sido cedidos por los usuarios del Grupo de Telegram _Grupo Baofeng DM-X DM 1701/1702_

### [CODEPLUG NACIONAL.data](https://gitlab.com/javifn/baofengdmx/-/raw/main/files/CODEPLUG%20NACIONAL.data?inline=false)

Por EA7KFF Juan (ALMERIA).

### [EA2DVG.data](https://gitlab.com/javifn/baofengdmx/-/raw/main/files/EA2DVG.data?inline=false)

Por EA2DVG. Cedido por el usuario _Luilli Pistamania_ (Nick del Grupo de Telegram).

### [Provincial hospot duplex.data](https://gitlab.com/javifn/baofengdmx/-/raw/main/files/Provincial%20hospot%20duplex.data?inline=false)

Por EA7KFF Juan (ALMERIA). Todas las provincias DM 1702 DM-X HOSPOT DUPLEX, SI TENEIS UN REPE CAMBIAS LA FRECUENCIAS Y A FUNCIONAR DE TX Y RX.

### [Codeplug RX 438.52500 TX 430.92500.data](https://gitlab.com/javifn/baofengdmx/-/raw/main/files/Codeplug%20RX%20438.52500%20TX%20430.92500.data?inline=false)

Codeplug completo. Lo que tenéis que cambiar en este codeplug es la frecuencia de recepción y de transmisión del repetidor.

Hay en analógico:

* los 16 canales de PMR
* los canales marítimos solo donde emiten
* los repetidos de la zona de las Rías Bajas

Las zonas en Digital están configuradas así:

* Los digitales nacionales donde hay movimiento
* Los digitales extranjeros-Hispanos donde hay movimiento
* Las zonas (EA1 EA2 etc) donde se transmite
* Los provinciales que en la red de Brandmeister hay transmisión

Finalmente donde más se ha trabajado en este codeplug es en meter en CONTACTOS todos los indicativos de la zona EA1 y parte de EA2 hasta llegar al límite de 800 que es lo que da nuestro "_ladrillo_".

### [Baofeng DM-X v4.data](https://gitlab.com/javifn/baofengdmx/-/raw/main/files/Baofeng%20DM-X%20v4.data?inline=false)

Por EA2EQQ Javier. Codeplug inicial de ejemplo.

### [D1.00.01.001.data](https://gitlab.com/javifn/baofengdmx/-/raw/main/files/D1.00.01.001.data?inline=false)

Por EA8DGL Esteban. Codeplug inicial de pruebas con dos zonas y dos canales en el Slot 1.

## Manuales

### Manual de usuario

Manual descargado de la página del fabricante.

[Manual de usuario (en inglés)](https://gitlab.com/javifn/baofengdmx/-/raw/main/files/DM-X%20USER'S%20MANUAL.pdf?inline=false)

### Manual de programación de Codeplug

Manual descargado de la página del fabricante.

[Programación de Codeplug (en inglés)](https://gitlab.com/javifn/baofengdmx/-/raw/main/files/DM-X%20Programming%20Guide.pdf?inline=false)

[Subir](#top)

# FAQ

Preguntas más frecuentes

## ¿Cuántos contactos puedo guardar?

Por ahora solo se pueden guardar 800 contactos. Se espera que en una próxima actualización de firmware esta cantidad aumente. Si quieres contactar con el fabricante para preguntarle cuando van a lanzar un nuevo firmware, puedes hacerlo en su página de contacto.
